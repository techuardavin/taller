//vamos a montar la lógica de los productos
/* function getproductos() {
  //necesitamos url conla que vamos a trabajar
  var url "http://services.odata.org/V4/Northwind/Northwind.svc/Products";
  var request = new XMLHttpRequest();
//onreadystatechange indica cuando cambia el estado de la peticin http
//cuando cambie el estado se ejecute el código de la funcion.
  request.onreadystatechange = function()
  {
    if(this.readyState == 4 && this.status== 200 ) {
      console.log(request.responseText);
      //comprobamos que hace correctamente la peticion
    }
  }
  request.open("GET",url,true);
  request.send();
  //el orden debe ser el que hemos puesto.
}*/

var productosObtenidos;

function gestionProductos(){
 var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Products"
 var request = new XMLHttpRequest();
//Definir datos de mi petición
 request.onreadystatechange = function()
 //cuando el objeto request haya cambiado su estado, se ejecute el sigueiente código
 {
   if(this.readyState == 4 && this.status == 200)
   {
     console.log(request.responseText);
     productosObtenidos=request.responseText;
     procesarProductos();
   }
 }
 request.open("GET",url,true);
 request.send();
}

function procesarProductos()
{
    var JSONProductos = JSON.parse(productosObtenidos);
     //busco la tabla
    var tabla= document.getElementById("tablaProductos");
    for (var i = 0; i < JSONProductos.value.length; i++) {
        var nuevaFila= document.createElement("tr");
        var columnaNombre = document.createElement("td");
        columnaNombre.innerText =JSONProductos.value[i].ProductName;
        var columnaPrecio = document.createElement("td");
        columnaPrecio.innerText =JSONProductos.value[i].UnitPrice;
        var columnaStock = document.createElement("td");
        columnaStock.innerText =JSONProductos.value[i].UnitsInStock;
        // por ahora solo la hemos creado
        nuevaFila.appendChild(columnaNombre);
        nuevaFila.appendChild(columnaPrecio);
        nuevaFila.appendChild(columnaStock);;
        tabla.appendChild(nuevaFila);
      //alert(JSONProductos.value[i].ProductName);
    }

}
